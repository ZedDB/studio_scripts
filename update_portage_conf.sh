#!/bin/bash

set -e

function Error()
{
  echo "$0: Error occured at line $1"
}
trap 'Error $LINENO' ERR

# We are syncing file with the git repo in a roundabout way because:
# 1. It works around the issue of having a git repo in /etc/portage
#    and we don't have to worry about it getting into an invalid state
# 2. The client computers can have their own configuration files.
#    If we for example used rsync, it would be much harder to allow for
#    custom configurations

GIT_REPO_URL=https://projects.blender.org/ZedDB/studio_portage_conf.git

if [ -d ~/studio_portage_conf ]
then
  cd ~/studio_portage_conf

  if [[ "$(git remote get-url origin)" != "$GIT_REPO_URL" ]]; then
    # Check if the default URL changed and update it if it no longer matches
    git remote set-url origin $GIT_REPO_URL
  fi

  # @{u} is a git shortcut for the current branch we are tracking
  git reset --hard @{u}
  old_hash=$(git rev-parse HEAD)
  git pull

  files_to_remove=$(git diff $old_hash...HEAD --name-status --diff-filter=DR | awk '{ print $2 }')
  cd /etc/portage/
  # Don't error when it can't remove files
  rm $files_to_remove || true
  # Remove empty folders if there are no files in there anymore (only relevant in the env and patches dir)
  find /etc/portage/patches/* -type d -empty -delete || true
  find /etc/portage/env/* -type d -empty -delete || true
else
  # Directory doesn't exist do a fresh git clone
  git clone $GIT_REPO_URL ~/studio_portage_conf
fi

cp -r ~/studio_portage_conf/* /etc/portage/
# Ensure that the zzz_autounmask files exists
touch /etc/portage/package.accept_keywords/zzz_autounmask
touch /etc/portage/package.use/zzz_autounmask
touch /etc/portage/package.mask/zzz_autounmask
