#!/bin/bash

if [ ! "$(ls /var/spool/nullmailer/queue/)" ]; then
  # The queue is empty, do nothing.
  exit 0
fi

SERVER_ADDR=$(cat /etc/studio_scripts/server_addr)

# Check if the server is up
ping -c1 -w3 -q $SERVER_ADDR
SUCCESS=$?

if [ $SUCCESS -eq 0 ]; then
  # Upload error messages and remove the queue if successful
  rsync /var/spool/nullmailer/queue/* rsync://$SERVER_ADDR/client-errors/$(hostname)/ && rm /var/spool/nullmailer/queue/*
fi
