#!/bin/bash

set -e

function Error()
{
  echo "$0: Error occured at line $1"
}
trap 'Error $LINENO' ERR

# We are syncing file with the git repo in a roundabout way because:
# 1. It works around the issue of having a git repo in /etc/cups
#    and we don't have to worry about it getting into an invalid state
# 2. We need to make sure that the cups service is not running when we
#    overwrite the config files.

# "|| true" because we don't want to error if we can't grep anything
service_was_stopped=$(/etc/init.d/cupsd status | grep "stopped" || true)

/etc/init.d/cupsd stop

GIT_REPO_URL=https://projects.blender.org/ZedDB/studio_printer_conf.git

if [ -d ~/studio_printer_conf ]
then
  cd ~/studio_printer_conf

  if [[ "$(git remote get-url origin)" != "$GIT_REPO_URL" ]]; then
    # Check if the default URL changed and update it if it no longer matches
    git remote set-url origin $GIT_REPO_URL
  fi

  # @{u} is a git shortcut for the current branch we are tracking
  git reset --hard @{u}
  old_hash=$(git rev-parse HEAD)
  git pull

  files_to_remove=$(git diff $old_hash...HEAD --name-status --diff-filter=DR | awk '{ print $2 }')
  cd /etc/portage/
  # Don't error when it can't remove files
  rm $files_to_remove || true
else
  # Directory doesn't exist do a fresh git clone
  git clone $GIT_REPO_URL ~/studio_printer_conf
fi

cp -r ~/studio_printer_conf/* /etc/cups/

if [ -z "$service_was_stopped" ]; then
  /etc/init.d/cupsd start
fi
