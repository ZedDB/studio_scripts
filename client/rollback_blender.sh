#!/bin/bash

# Check if the /usr/bin/blender symlink exists and rollback the package it points to
if [ -f /usr/bin/blender ]; then
  blender_package_name=$(equery -q b /usr/bin/blender | grep -v blender-sym)
else
  # Fallback to the live blender ebuild
  blender_package_name=media-gfx/blender-9999
fi

installed_id=$(cat /var/db/pkg/$blender_package_name/BUILD_ID)

package_file_path="/var/cache/binpkgs/Packages"

source /etc/portage/make.conf

if [ -n "$PORTAGE_BINHOST" ]; then
  wget $PORTAGE_BINHOST/Packages -O /tmp/Packages && package_file_path="/tmp/Packages" || echo "Falling back to local binary file cache"
fi

builds=$(grep --no-group-separator -B 2 "CPV: $blender_package_name" $package_file_path \
        | sed '/blender/d' \
        | sed 's/BUILD_ID: //' \
        | sed 's/BUILD_TIME: //' \
        )
echo -e "Available builds:\n"

valid_ids=()

ITER=0
is_cur_ver=0
for info in $builds; do
  if [ $(expr $ITER % 2) == 0 ]; then
    if [ $info == $installed_id ]; then
      printf "\e[1mID:\e[0m \e[100m%3s " $info
      is_cur_ver=1
    else
      printf "\e[1mID:\e[0m %3s " $info
    fi
    valid_ids+=($info)
  else
    printf "($(date --date=@$info +%F" "%H:%M))"
    if [ $is_cur_ver == 1 ]; then
      printf " <installed>\e[0m"
      is_cur_ver=0
    fi
    echo
  fi
  ITER=$(expr $ITER + 1)
done

echo -e "\n"

choosen_blend_id=-1

prompt="Select which Blender build number to switch to. (press ENTER to confirm): "
while read -rp "$prompt" num && [[ "$num" ]]; do

  # Check if "num" is a valid number.
  [[ "$num" != *[[:digit:]]* ]] &&
    { echo "You need to choose a number!"; continue; }

  if [[ ! " ${valid_ids[*]} " =~ " ${num} " ]]; then
    # whatever you want to do when array doesn't contain value
    echo "$num is not an available ID!"
    continue
  fi

  choosen_blend_id=$num

  break
done

((choosen_blend_id < 0)) && exit 0

emerge =$blender_package_name-$choosen_blend_id
