#!/bin/bash

# Get names of logged in users
user_list=$(who | awk '{print $1}' | sort -u)

process_name="dbus-daemon --config-file=/usr/share/defaults/at-spi2/accessibility.conf"

# Make sure that the arguments are correctly escaped
arguments=$(printf " %q" "$@")

for user in $user_list; do
  # Get the pid of a process that will have the DBUS_SESSION_BUS_ADDRESS variable
  DBUS_PID=`ps ax --user $user | grep "$process_name" | grep -v grep | awk '{ print $1 }'`

  if [ -z "$DBUS_PID" ]; then
    continue
  fi

  # Get DBUS_SESSION_BUS_ADDRESS variable (and remove nullbytes from the output to avoid warnings)
  DBUS_SESSION=`grep -z DBUS_SESSION_BUS_ADDRESS /proc/$DBUS_PID/environ | tr -d '\0' | sed -e s/DBUS_SESSION_BUS_ADDRESS=//`

  # Send notification
  su - $user -c "DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION notify-send $arguments"
done

# Ensure that we don't stop the update script if we can't send the notification
exit 0
