#!/bin/bash

# Exit on command errors
set -e

function Error()
{
  echo "$0: Error occured at line $1"
  if [ -n "$server_time_stamp" ]; then
    # Make sure that we update our timestamp on errors so we don't attempt to update
    # until the issue has been fixed.
    date -d @$server_time_stamp -R > /var/db/studio_scripts/timestamp.chk
  fi
}
trap 'Error $LINENO' ERR

echo_info() {
  # Print in bold
  echo -e "\e[1m$@\e[00m"
}

while getopts 's' OPTION; do
  case "$OPTION" in
    s)
      echo_info Skipping update timestamp check!
      echo
      skip_timestamp_check="yes"
      ;;
    ?)
      echo "script usage: $(basename \$0) [-s]" >&2
      echo
      echo This script will check if there are any updates on the build server
      echo and download and install them if that is the case.
      echo
      echo Options:
      echo -e ' '-s'\t' Skip any update timestamp checks and attempt to update regardless
      exit 1
      ;;
  esac
done
shift "$(($OPTIND -1))"

if [ ! -f /var/db/studio_scripts/timestamp.chk ]; then
  # We do not have a timestamp file. Copy in the portage timestamp and use that.
  cp /var/db/repos/gentoo/metadata/timestamp.chk /var/db/studio_scripts/
fi

SERVER_ADDR=$(cat /etc/studio_scripts/server_addr)

client_time_stamp=$(date -d "$(cat /var/db/studio_scripts/timestamp.chk)" +%s)
server_time_stamp=$(date -d "$(curl --connect-timeout 5 -fsS http://$SERVER_ADDR/update_info/timestamp.chk || echo error)" +%s || echo 0)

if [[ "$server_time_stamp" -eq 0 ]]; then
  echo Could not connect to the build server, exiting.
  exit 0
fi

if [[ ! ${skip_timestamp_check} && ! "$server_time_stamp" -gt "$client_time_stamp" ]]; then
  echo We are already up to date!
  exit 0
fi

# Make sure we have the same PATH variable as when using a regular shell
# Otherwise some commands that are in sbin like etc-update will not be found
# when executing this via cron
source /etc/profile.env

# Check if the current computer has been woken up for updates and should be turned off after we finish.
try_to_shutdown=$(comm -12 <(curl -fsS http://$SERVER_ADDR/update_info/wol_macs.txt | sort) <(cat /sys/class/net/*/address | sort))
# Check if the system has a uptime of less than 10 minutes
recently_started=$(awk '$1 < (10 * 60) { print "yes" }' /proc/uptime)

#notify.sh -t 0 -u critical -a "Automatic Update" "System updating, don't turn off computer!"

update_portage_conf.sh
update_printer_conf.sh

eix-sync

# Update portage version first if there are any updates
emerge -1u --rebuilt-binaries portage

# Besides just updating to newer versions, also re-emerge any binary packages that has a newer build date on the server
# One can also pass --rebuilt-binaries-timestamp to limit the rebuilds to after a given timestamp
#
# Also pass --with-bdeps=y to ensure that all clients can rebuild their packages when not connected to the internet.
emerge -Du --newuse --rebuilt-binaries --with-bdeps=y world

# Update our timestamp file, we have managed to install the updates.
date -d @$server_time_stamp -R > /var/db/studio_scripts/timestamp.chk

# Clean up old packages and their installation/source files
emerge --depclean
eclean-dist -d
eclean-pkg -d -t 2w

# Decline all config updates
echo "-9
YES
" | etc-update

#notify.sh -t 0 -u critical -a "Automatic Update" "System update completed"
dump_hw_to_server.sh

if [[ "$try_to_shutdown" ]]; then
  # We should try to shut down the computer, but check first if there is anyone logged into it.
  if [[ -z "$(who)" ]]; then
    # No one is logged in, power it off.
    /sbin/poweroff
  elif [[ "$recently_started" ]]; then
    # In case the computer has autologin turned on, post a notification to see if anyone is actually using the computer before shuting it down.
    # Not fool proof, but there doesn't seem to be any better way to solve this.
    wait_min=5
    # We need the "|| true" at the end to catch the error code if we timeout. set -e will stop the script execution otherwise.
    should_shutdown=$(timeout ${wait_min}m notify.sh -u critical -A no="Abort shut down" "The computer will shutdown in $wait_min minutes" "Dismissing this popup without choosing to abort will shut down the computer immedietly" || true)
    if [[ "$should_shutdown" != "no" ]]; then
      /sbin/poweroff
    fi
  fi
fi
