#!/bin/bash

SERVER_ADDR=$(cat /etc/studio_scripts/server_addr)

# Check if the server is up
ping -c1 -w3 -q $SERVER_ADDR
SUCCESS=$?

if [ $SUCCESS -eq 0 ]; then
  # Make sure we have the same PATH variable as when using a regular shell
  # Otherwise some commands that are in sbin like hdparm will not be found
  # when executing this via cron
  source /etc/profile.env
  mkdir -p /tmp/hw_data/
  hw_script.sh /tmp/hw_data/$(hostname)

  # Upload to server
  rsync /tmp/hw_data/$(hostname) rsync://$SERVER_ADDR/client-hw-info/
  # Cleanup
  rm -fr /tmp/hw_data
fi
