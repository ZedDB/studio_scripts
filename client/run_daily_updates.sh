#!/bin/bash

# Exit on command errors
set -e

emerge -u --rebuilt-binaries @blender-set
# Make sure that we don't keep any installation packages that are older than 2 weeks
eclean-pkg -d -t 2w
