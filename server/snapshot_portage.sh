#!/bin/bash

# Exit on command errors
set -e

echo_info() {
  # Print in bold
  echo -e "\e[1m$@\e[00m"
}

while getopts 'hr' OPTION; do
  case "$OPTION" in
    r)
      echo_info Restoring portage snapshot
      echo
      restore_snapshot="yes"
      ;;
    ?)
      echo "script usage: $(basename $0) [-r]" >&2
      echo
      echo This script will create a snapshot of the gentoo repo in
      echo /var/db/repos/gentoo. The snapshot will be stored in the
      echo current working directory.
      echo
      echo Options:
      echo -e ' '-r'\t' Restore the snapshot in the current working dir
      exit 1
      ;;
  esac
done
shift "$(($OPTIND -1))"

if [[ ! "$restore_snapshot" ]]; then
  # Create snapshot
  echo Creating portage snapshot portage_snapshot.tar.gz 
  tar -cvpzf portage_snapshot.tar.gz -C /var/db/repos gentoo
else
  # Restore snapshot
  echo Restoring portage snapshot from portage_snapshot.tar.gz 
  rm -fr /var/db/repos/gentoo
  tar -xvpzf portage_snapshot.tar.gz -C /var/db/repos
fi
