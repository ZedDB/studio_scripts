#!/bin/bash

# Stop execution if any command fails
set -e

function Error()
{
  echo "$0: Error occured at line $1"
}
trap 'Error $LINENO' ERR

cd /var/cache/stage3/

echo "Trying to grab Gentoo releng & infrastructure gpg key..."
gpg --locate-key releng@gentoo.org; gpg --locate-key infrastructure@gentoo.org

DISTMIRROR=http://distfiles.gentoo.org
DISTBASE=${DISTMIRROR}/releases/amd64/autobuilds/current-stage3-amd64-desktop-openrc/
FILE=$(wget -q "${DISTBASE}latest-stage3-amd64-desktop-openrc.txt" -O - | grep -e '-desktop-' | cut -d " " -f 1 | head -1)
[ -z "$FILE" ] && echo No stage3 found on $DISTBASE && exit 1
[ -f $FILE ] && echo $FILE already exists, we seem to be up to date! && exit 0
echo Downloading latest stage file $FILE

# Remove old stage files
rm * || true

wget $DISTBASE$FILE
wget $DISTBASE$FILE.DIGESTS
wget ${DISTBASE}latest-stage3-amd64-desktop-openrc.txt

gpg --verify $FILE.DIGESTS
echo "Verifying stage3 SHA512 ..."
# grab SHA512 lines and line after, then filter out line that ends with iso
echo "$(grep -A1 SHA512 $FILE.DIGESTS | grep $FILE\$)" | sha512sum -c || bash
echo " - Awesome! stage3 verification looks good."
