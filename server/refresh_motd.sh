#!/bin/bash

client_errors=$(ls /var/studio_scripts/errors/ | wc -l)
server_errors=$(ls /var/spool/nullmailer/queue/ | wc -l)

total_errors=$((client_errors + server_errors))

# Don't print anything if there are no unhandled error logs
if [ $total_errors -eq 0 ]; then
  # Remove the old motd file if it exists
  rm -f /etc/motd
  exit 0
fi

start_mess="Attention! There are unhandled error logs on this system!"
#Print with inverted text to grab attention
echo -e "\033[7m$start_mess\033[0m" > /etc/motd

if [ $client_errors -ne 0 ]; then
  echo There are $client_errors unhandled error log folders in /var/studio_scripts/errors/ >> /etc/motd
fi
if [ $server_errors -ne 0 ]; then
  echo There are $server_errors unhandled error logs in /var/spool/nullmailer/queue/ >> /etc/motd
fi
