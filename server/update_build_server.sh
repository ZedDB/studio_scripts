#!/bin/bash

# Exit on command errors
set -e

function Error()
{
  echo "$0: Error occured at line $1"
}
trap 'Error $LINENO' ERR

echo_info() {
  # Print in bold
  echo -e "\e[1m$@\e[00m"
}

while getopts 'hs' OPTION; do
  case "$OPTION" in
    s)
      echo_info Skipping live ebuild update check
      echo
      skip_live_update="yes"
      ;;
    ?)
      echo "script usage: $(basename $0) [-s]" >&2
      echo
      echo This script will update all packages on the build server.
      echo It will ask once the update is done if you want to push out
      echo the updated packages to the client computers.
      echo
      echo Options:
      echo -e ' '-s'\t' Do not check for live ebuild updates
      exit 1
      ;;
  esac
done
shift "$(($OPTIND -1))"

# Save data for client package use flag rebuild later
start_date=$(date +%s)

update_portage_conf.sh

eix-sync
# Update portage version first if there are any updates
emerge -1u portage
# Update the rest of the packages
emerge -avDu --newuse --changed-deps world

# Save emerged live packages to avoid the posibility of re-emerging them again in the "smart-live-rebuild" call
live_packages_emerged=$(qlop -v -d $start_date | grep 9999 | awk '{print $3}')
exclude_packages=""
if [ "$live_packages_emerged" ]; then
  exclude_packages=$(echo "$live_packages_emerged" | sed -e "s/^/ -f '\!/" | sed -e "s/-9999.*://" | tr "\n" "'")
fi

[[ ! "$skip_live_update" ]] && echo $exclude_packages | xargs smart-live-rebuild

emerge --depclean
emerge @preserved-rebuild

eclean-dist -d
eclean-pkg -d -t 2w

# Rebuild packages without server specific flags for the clients

# Get the first columns of the client use file, strip out all commented and empty lines,
# convert all newlines to "|" for grep, strip trailing spaces with xargs
grep_packages=$(awk '{print $1}' /etc/studio_scripts/client_use | grep -Ev "^#|^$" | tr '\n' '|' | xargs)
# Did any of these get emerged during this update?
client_packages=$(qlop -d $start_date | grep -Eo $grep_packages | tr '\n' ' ')

if [ "$client_packages" ]; then
  # Create a symlink with to the client specific useflags. The filename is choosen as to make it the last file in the folder naming wise.
  # This makes it so that the client useflags is applied last in the stack.
  ln -s /etc/studio_scripts/client_use /etc/portage/package.use/zzzz_temp_client
  # Create binary packages for the client computers without installing then on the server.
  emerge -1B $client_packages

  # Remove the symlink
  rm /etc/portage/package.use/zzzz_temp_client
fi

echo
echo Updates installed, do you want to roll out the updates to the clients?
echo

select yn in "Yes" "No"; do
  case $yn in
    # Update timestamp sync file that is retrived by the clients.
    # If the timestamp is newer than their latest sync timestamp, they will update
    Yes )
      ~/signal_updates.sh
      break;;
    No )
      exit;;
    *)
      echo "invalid option $REPLY";;
  esac
done
