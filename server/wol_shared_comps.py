#!/usr/bin/env python

import json
import re
import socket
import struct
import sys
import subprocess
import signal
from time import sleep

debug = True

def signal_handler(_sig, _frame):
    # Ensure that we restore the cursor
    print('\033[?25h', end="")
    # Remove the mac WOL list
    subprocess.run('rm /var/cache/update_info/wol_macs.txt', shell=True)
    sys.exit(0)

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def is_host_online(host):
    command = ["ping", "-c", "1", "-w2", host]

    if debug:
        print(", " + host + ": ", end='')

    is_online = subprocess.run(args=command, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL).returncode == 0

    if debug:
        if is_online:
            print(bcolors.OKGREEN + "Online" + bcolors.ENDC)
        else:
            print(bcolors.WARNING + "Offline" + bcolors.ENDC)

    return is_online

def wake_up_computers(mac_adresses):
    # Taken from https://github.com/bentasker/Wake-On-Lan-Python
    for mac in mac_adresses:
        mac_orig = mac
        # Check mac address format
        found = re.fullmatch(
            '^([A-F0-9]{2}(([:][A-F0-9]{2}){5}|([-][A-F0-9]{2}){5})|([s][A-F0-9]{2}){5})|([a-f0-9]{2}(([:][a-f0-9]{2}){'
            '5}|([-][a-f0-9]{2}){5}|([s][a-f0-9]{2}){5}))$',
            mac)

        # We must found 1 match , or the MAC is invalid
        if found:
            # If the match is found, remove mac separator [:-\s]
            mac = mac.replace(mac[2], '')
        else:
            raise ValueError('Incorrect MAC address format: ' + mac_orig)

        # Pad the synchronization stream.
        data = ''.join(['FFFFFFFFFFFF', mac * 20])
        send_data = b''

        # Split up the hex values and pack.
        for j in range(0, len(data), 2):
            send_data = b''.join([
                send_data,
                struct.pack('B', int(data[j: j + 2], 16))
            ])

        # Broadcast it to the LAN.
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        sock.sendto(send_data, ("255.255.255.255", 7))
        if debug:
            print("Sent magic packet for: " + mac_orig)

# Main start
if len(sys.argv) < 2:
    print("You need to provide files to parse!")
    sys.exit()

computers = list()

# Get all computers that we should potentially try to wakeup
for input_file in sys.argv[1:]:
    with open(input_file, 'r') as data_file:
        data = data_file.read()

    computers.append( json.loads(data) )

mac_adresses = list()

# Check which computers are asleep and record their mac adresses
for comp in computers:
    if debug:
        processing_str = "Processing: " + comp["Hostname"]
        print(processing_str, end='')
    first_net_interface = True
    for net in comp["Network"]:
        ip = net["IP"]
        if ip == "DOWN":
            # Skip network interfaces that were down when the machine phoned home
            continue
        if debug and not first_net_interface:
            #Apply padding for next ip adress print
            print(" "*len(processing_str), end='')
        if not is_host_online(ip):
            # Add mac adresses of offline computers
            mac_adresses.append(net["MAC"])
        first_net_interface = False

# Ask user if we should proceeed.
user_input = input('Wake up the offline computers? (yes/{}no{}): '.format(bcolors.BOLD, bcolors.ENDC))

yes_choices = ['yes', 'y']

if not user_input.lower() in yes_choices:
    print('Canceling WOL...')
    sys.exit()

# Write the mac adresses to disk
out_file = "/tmp/wol_macs.txt"
try:
    f_out = open(out_file,"w")
except:
    print("Couldn't open file " + out_file + " for writing")
    sys.exit()

for mac in mac_adresses:
    f_out.write(mac + "\n")

f_out.close()

# Expose the mac list on the server so the computers can check if they should turn off after the update
subprocess.run('mv /tmp/wol_macs.txt  /var/cache/update_info/', shell=True)

wake_up_computers(mac_adresses)

signal.signal(signal.SIGINT, signal_handler)
# Hide the text cursor
print('\033[?25l', end="")

# Sleep for 10 minutes
print("Sleeping for:")
sleep_time = 10 * 60
for i in range(sleep_time,0,-1):
    print(f"{i//60} min {i%60} sec  ", end="\r", flush=True)
    sleep(1)

# Remove the mac WOL list
subprocess.run('rm /var/cache/update_info/wol_macs.txt', shell=True)

# Show the text cursor again
print('\033[?25h', end="")
